**Table of Contents**:

- [Install Go](#install-go)
- [Optionally Install LuaJIT](#install-luajit)
- [Get the project](#get-the-project)

## Install Go

  Follow the instructions at https://golang.org/doc/install

## Install LuaJIT

LuaJIT is optional if you are not going to use Lua scripting. But it is highly recommended. If you like Go, I think you will love Lua once you are familiar with it.

### Install LuaJIT

Download LuaJIT and install.

```sh
  $ wget http://luajit.org/download/LuaJIT-2.0.4.tar.gz
  $ tar zxf LuaJIT-2.0.4.tar.gz
  $ cd LuaJIT-2.0.4
  $ sudo make install
```

### Install MessagePack lib

Put this customized MessagePack.lua under a folder where luajit can find it.

Option 1: copy a file to lua lib path
```sh
  $ wget https://raw.githubusercontent.com/chrislusf/gleam/master/script/MessagePack.lua
  $ sudo cp MessagePack.lua /usr/local/share/luajit-2.0.4/
```

Option 2: install for the machine
```sh
  $ sudo luarocks install --server=http://luarocks.org/dev gleam-lua
```

Option 3: or install it locally
```sh
  $ luarocks install --server=http://luarocks.org/dev gleam-lua --local
```

### Test Installation
```
  luajit -e "require 'MessagePack'"
```
If it is complaining something like this, just copy the MessagePack.lua to one of the paths.
```
luajit: (command line):1: module 'MessagePack' not found:
	no field package.preload['MessagePack']
	no file './MessagePack.lua'
	no file '/usr/local/share/luajit-2.0.4/MessagePack.lua'
	no file '/usr/local/share/lua/5.1/MessagePack.lua'
	no file '/usr/local/share/lua/5.1/MessagePack/init.lua'
	no file './MessagePack.so'
	no file '/usr/local/lib/lua/5.1/MessagePack.so'
	no file '/usr/local/lib/lua/5.1/loadall.so'
```

## Get the project

```sh
$ go get github.com/chrislusf/gleamold/flow
$ go get github.com/chrislusf/gleamold/distributed/gleamold
```
