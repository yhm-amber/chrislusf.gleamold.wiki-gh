* Architecture
 * [[Why Gleamold? | why gleamold]]
 * [[Write Mapper Reducer in Go | Write Mapper Reducer in Go]]

* Setup
 * [[Installation | Installation]]
 * [[Gleamold Cluster Setup | Gleamold Cluster Setup]]

* Gleamold APIs
 * [[Source() Listen() TextFile() Channel() Strings() Bytes() Ints() | Source ]]
 * [[Pipe() and PipeArgs() | Pipe-And-PipeArgs]]
 * [[Partition() | Partition ]]
 * [[Map() ForEach() FlatMap() Filter() Select() | Map]]
 * [[Reduce() ReduceBy()| Reduce]]
 * [[Sort() SortBy() | Sort]]
 * [[Join() CoGroup() GroupBy()| Join-And-CoGroup]]
 * [[Output() Fprintf() | Output]]

* Data Sources
 * [[CSV File | CSV]]

* Examples
 * [[Joined Word Count | Joined-Word-Count]]
 * [[Join CSV Files | Join CSV Files]]
 * [[TF-IDF | TF-IDF]]